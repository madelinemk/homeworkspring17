;; Madeline Kusters
;; Programming Paradigms Assignment 2

(define list-size
	(lambda (l)
		(cond ((null? l) 0)
					(else (+ 1 (list-size (cdr l)))))))

(define remove-falses
	(lambda (l)
		(cond ((null? l) '())
					((equal? #f (car l)) (remove-falses (cdr l)))
					(else (cons (car l) (remove-falses (cdr l)))))))

(define add-falses
	(lambda (l)
		(cond ((equal? 4 (list-size l)) l)
					(else (add-falses (cons #f l))))))

(define combine-numbers
	(lambda (l)
		(cond ((null? l) '())
					((equal? 1 (list-size l)) l)
					((equal? (car l) (car (cdr l))) (cons (* 2 (car l)) (combine-numbers (cdr (cdr l)))))
					(else (cons (car l) (combine-numbers (cdr l)))))))

(define move-right-row
	(lambda (l)
		(add-falses (combine-numbers (remove-falses l)))))

;; tests
(display "\nTests\nmove-right-row '(#f 2 #f #f)\n")
(display (move-right-row '(#f 2 #f #f)))
(display "\n")

(display "move-right-row '(2 2 #f 4)\n")
(display (move-right-row '(2 2 #f 4)))
(display "\n")

(display "move-right-row '(2 2 2 2)\n")
(display (move-right-row '(2 2 2 2)))
(display "\n\n")


(define move-right
	(lambda (board)
		(cond ((null? board) '())
					(else (cons (move-right-row (car board)) (move-right (cdr board)))))))

;;tests
(display "testing move-right on a board completely of 2s\n")
(display (move-right '( (2 2 2 2) (2 2 2 2) (2 2 2 2) (2 2 2 2))))
(display "\n\n")

(display "testing move-right on:\n(#f 2 #f #f)\n(2 2 #f 4)\n(2 2 2 2)\n(4 2 2 2)\n\n")
(define board '((#f 2 #f #f) (2 2 #f 4) (2 2 2 2) (4 2 2 2)))
(display (move-right board))
(display "\n\n")



