#
# Madeline Kusters
#

import TwoP2048

gameboard = TwoP2048.board()

# initialize board with 2 2s
gameboard.computer_update()
gameboard.computer_update()

print("2048\n====")
print(gameboard)
print()

done = False
player1 = 0
player2 = 0

while not done:

  print("Player 1 score: {} \tPlayer 2 score: {}\n".format(player1, player2))

  # player 1
  direction = input("Player 1 move (l, r, u, d): ")
  (score_increase, new_board) = gameboard.move(direction)

  #update score and board
  player1 += score_increase
  gameboard = new_board
  gameboard.computer_update()
  print(gameboard)

  #check for end game
  if gameboard.isfull() and len(gameboard.expand()) == 0:
    print("Game Over")
    done = True
    continue



  #player 2
  direction = input("Player 2 move (l, r, u, d): ")
  (score_increase, new_board) = gameboard.move(direction)

  #update board
  player2 += score_increase
  gameboard = new_board
  gameboard.computer_update()
  print(gameboard)

  #check for end game
  if gameboard.isfull() and len(gameboard.expand()) == 0:
    print("Game Over")
    done = True
    continue


print("Player 1 score: {} \tPlayer 2 score: {}\n".format(player1, player2))
if (player1 > player2):
  print("Player 1 wins!")
elif (player1 < player2):
  print("Player 2 wins!")
else:
  print("It's a tie!")






