#
# Madeline Kusters
#

import random
import functools

#
#
class board:
  """A class to manage a two-player 2048 board."""

  # this embodies the search pattern for empty cells.
  spiral = [[1,1],[1,2],[2,2],[2,1],[2,0],[1,0],[0,0],[0,1],[0,2],[0,3],[1,3],[2,3],[3,3],[3,2],[3,1],[3,0]]

  def __init__(self):
    """initialize the board."""
    self.board = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

  def __str__(self):
    res = ""
    for row in self.board:
      for item in row:
        res += str(item) + " "
      res += "\n"
    res = res[:-1]
    return res

  def newgame(self):
    """reinitialize the board."""
    __init__(self)

  def set_board(self,b):
    self.board = b.copy()

  def isfull(self):
    """returns False if there's an empty cell."""
    flat = functools.reduce(lambda a,b: a+b, self.board)
    return not 0 in flat

  def computer_update(self):
    empty = self.emptyindex()
    self.place(2, empty)

  def emptyindex(self):
    """return the 2D index of the first empty cell."""
    for entry in self.spiral:
      if self.board[entry[0]][entry[1]] == 0:
        return entry
    return False

  def place(self,n,loc):
    """place an n in the specified location location [r,c]."""
    self.board[loc[0]][loc[1]] = n
    return 0

  def flip(self):
    """flip the board left-to-right."""
    # legit one-liner
    self.board = [x[::-1] for x in self.board]

  def transpose(self):
    """transpose the board."""
    self.board = [list(t) for t in list(zip(*self.board))]

  def move(self, dir):
    if (dir == 'l'):
      return self.move_left()
    elif (dir == 'r'):
      return self.move_right()
    elif (dir == 'u'):
      return self.move_up()
    elif (dir == 'd'):
      return self.move_down()
    else:
      print("Invalid direction")
      return [False, False]


  def move_right(self):
    """move right; return [score increase, new_board]. Do not modify this board."""
    s = 0
    nb = []
    for r in self.board:
      r2 = [x for x in r if x != 0] # collapse zeros
      r3 = []
      if len(r2) >= 2:
        while len(r2) > 1:
          if r2[0] == r2[1]:
            r3 = r3 + [2*r2[0]]
            s = s + 2*r2[0]
            r2 = r2[2:]
          else:
            r3 = r3 + [r2[0]]
            r2 = r2[1:]
      if len(r2) == 1:
        r3 = r3 + [r2[0]]
      r3 = [0,0,0,0]+r3
      r3 = r3[-4:]
      nb.append(r3)
    b_out = board()
    b_out.set_board(nb)
    return [s,b_out]

  def move_left(self):
    """move left; return score increase."""
    # rotate board 180 degrees
    self.flip()
    # move_right
    (score, nb) = self.move_right()
    # rotate board back
    self.flip()
    nb.flip()
    return [score, nb]

  def move_up(self):
    """move up; return score increase."""
    self.transpose()
    (score, nb) = self.move_left()
    nb.transpose()
    self.transpose()
    return [score, nb]

  def move_down(self):
    """move down; return score increase."""
    self.transpose()
    (score, nb) = self.move_right()
    nb.transpose()
    self.transpose()
    return [score, nb]

  def expand(self):
    """Expand current state into all possible sucessor states: return list of successor boards."""
    # assume "move_foo" methods return (score, board).
    [s_right,b_right] = self.move_right()
    [s_left,b_left] = self.move_left()
    [s_up,b_up] = self.move_up()
    [s_down,b_down] = self.move_down()

    res = []
    if s_right >= 0: res.append(b_right)
    if s_left >= 0: res.append(b_left)
    if s_up >= 0: res.append(b_up)
    if s_down >= 0: res.append(b_down)

    return res


