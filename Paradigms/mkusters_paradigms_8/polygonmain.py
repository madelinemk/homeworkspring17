import pyglet
from pyglet.window import key
import random
import polygon

window = pyglet.window.Window(height=512, width=512)

poly = polygon.Polygon(window)

@window.event
def on_draw():
  window.clear()
  poly.draw()

def tickall(dt):
  poly.tick()

pyglet.clock.schedule_interval(tickall, 0.05)
pyglet.app.run()
