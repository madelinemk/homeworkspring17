import pyglet
import random

class Polygon:
  """A 10-sided polygon class that is animated and draws itself"""

  def __init__(self,window=[]):
    self.window=window
    self.v = []
    self.velocity = []
    w, h = window.get_size()
    for i in range(10):
      self.v.append([random.random()*w, random.random()*h])
      self.velocity.append([random.random()*10-5, random.random()*10-5])

  def tick(self):
    for i in range(10):
      newx = self.v[i][0] + self.velocity[i][0]
      newy = self.v[i][1] + self.velocity[i][1]
      # check for out of bounds
      if (newx >= 512) or (newx <= 0):
        self.velocity[i][0] *= -1
        newx = self.v[i][0] + self.velocity[i][0]
      if (newy >= 512) or (newy <= 0):
        self.velocity[i][1] *= -1
        newy = self.v[i][1] + self.velocity[i][1]

      self.v[i][0] = newx
      self.v[i][1] = newy


  def draw(self):
    pyglet.gl.glColor3f(1.0,1.0,0.0)
    vl = []
    for ix in [0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,0]:
      vl.append(self.v[ix][0])
      vl.append(self.v[ix][1])

    tvl = tuple(vl)
    pyglet.graphics.draw(20, pyglet.gl.GL_LINES,('v2f', tvl))
