import csv
import sys
import os
import random
import math

from flask import Flask
from flask import render_template
from flask import request

import json
import pickle

# load the food data -- from the pickle file if it exists.
# if it doesn't exist, load from csv and pickle the result.


if os.path.exists('Food_Inspections.pkl'):
  print('unpickling data')
  with open('Food_Inspections.pkl','rb') as f:
    data = pickle.load(f)
else:
  data = []
  with open('Food_Inspections.csv','r',newline=None,encoding='ISO-8859-1') as f:
    r = csv.DictReader(f)
    for line in r:
      if (line['Latitude'] == '') or (line['Longitude'] == '') or (line['Zip'] == ''):
        continue
      for label in ['Latitude','Longitude']:
        #print(line[label])
        line[label] = float(line[label])
      data.append(line)
  print('pickling data')
  with open('Food_Inspections.pkl','wb') as f:
    pickle.dump(data,f,pickle.HIGHEST_PROTOCOL)

# pull out essential income data (6 records per zip code based on AGI level)
incometmp = []
keepkeys = ['zipcode','agi_stub','N1','mars1','MARS2','MARS4','NUMDEP','A00100','A06500']

#
# load and, if necessary, pickle the income data
if os.path.exists('ILincome.pkl'):
  print('unpickling income')
  with open('ILincome.pkl','rb') as f:
    agg = pickle.load(f)
else:
  with open('ILincome.csv','r',newline=None,encoding='ISO-8859-1') as f:
    r = csv.DictReader(f)
    for line in r:
      for k in list(line.keys()):
        if k not in keepkeys: del line[k]
      incometmp.append(line)

# now boil the income data down into one entry per zip code
  agg = []
  zips = set([x['zipcode'] for x in incometmp])
  #print('zips: {}'.format(zips))
  for z in zips:
    a = {'zipcode':z}
    for i in incometmp:
      if i['zipcode'] == z:
        for k in i:
          if k in keepkeys:
            if k in ['zipcode','agi_stub']: continue
            if k not in a:
              a[k] = float(i[k])
            else:
              a[k] = a[k] + float(i[k])
    agg.append(a)
  print('pickling income')
  with open('ILincome.pkl','wb') as f:
    pickle.dump(agg,f,pickle.HIGHEST_PROTOCOL)

# now, agg = [ {'zipcode': zip, 'N1':sum_of_N1s, 'MARS1': sum_of_MARS1s, ...}, ...]


# sanity check: make sure every zip in inspection table is present in income summary
# note we use zips later
zips = set([x['zipcode'] for x in agg])
izips = set([x['Zip'] for x in data])
for z in izips:
  #print('checking {}'.format(z))
  if z not in zips:
    print('Note: zipcode {} present in inspections but not in incomes. Removing from inspections.'.format(z))
    d = [x for x in data if x['Zip'] != z]
    data = d

izips = set([x['Zip'] for x in data])


app = Flask(__name__)

@app.route('/',methods=['GET'])
def index():
  """Print two key statistics as an I'm alive test."""
  n1 = len(data)
  n2 = len(agg)
  return json.dumps({"num_inspections":n1, "num_incomesummaries":n2}),200

@app.route('/zips',methods=['GET'])
def getzips():
  """Return a list of unique zip codes in the inspection data."""
  izips = set([x['Zip'] for x in data])
  return json.dumps(list(izips)),200

@app.route('/inspectionkeys',methods=['GET'])
def inspectionkeys():
  """Return the list of keys for the inspection dictionaries."""
  return json.dumps(list(data[0].keys())),200

@app.route('/incomekeys',methods=['GET'])
def incomekeys():
  """Return the list of keys for the income dictionaries."""
  ks = list(agg[0].keys())
  return json.dumps(ks),200

@app.route('/inspectionszip/<z>',methods=['GET'])
def inspectionszip(z):
  """Return the inspections for a specific zip code."""
  d = [x for x in data if x['Zip'] == z]
  return json.dumps(d),200

@app.route('/inspections',methods=['POST','DELETE'])
def inspections():
  """GET (disabled): returns a list of all inspections. POST: add a new inspection record. DELETE: deletes all inspections."""
  global data
  if request.method == 'GET':
    return json.dumps(data),200
  elif request.method == 'POST':
    b = json.loads(request.get_data())
    print('body: {}'.format(b))
    # scan for correctness?
    ids = [x['Inspection ID'] for x in data]
    flag = True
    while flag:
      newid = str(random.randint(1000000,10000000))
      flag = newid in ids
    b['Inspection ID']=newid
    data.append(b)
    return json.dumps({"result":"success","id":newid}),200
  elif request.method == 'DELETE':
    data = []
    return json.dumps({"result":"success"}),200

@app.route('/inspections/<insp_id>',methods=['GET','DELETE','PUT','POST'])
def inspection(insp_id):
  """GET: get insp with specified ID. DELETE: delete insp with specified ID. PUT: update existing insp. POST: does nothing."""
  global data
  if request.method == 'GET':
    d = [x for x in data if x['Inspection ID'] == insp_id]
    return json.dumps(d),200
  elif request.method == 'DELETE':
    d = [x for x in data if x['Inspection ID'] != insp_id]
    if len(d) == len(data):
      return json.dumps({"result":"error"}),501
    data = d
    return json.dumps({"result","success"}),204
  elif request.method == 'PUT':
    record = [x for x in data if x['Inspection ID'] == insp_id]
    if (len(record) == 0) or (len(record) > 1):
      return 'Error: {} records matched ID {}.'.format(len(record),insp_id),501
    record = record[0]
    b = json.loads(request.get_data())
    print('body: {}'.format(b))
    for k in b:
      if k == 'Inspection ID': continue
      record[k] = b[k]
    d = [x for x in data if x['Inspection ID'] != insp_id]
    d.append(record)
    data = d
    return '{"result","success"}',200
  else: # request.method is 'POST'
    pass

@app.route('/incomes',methods=['GET'])
def incomes():
  """Returns income records."""
  return json.dumps(agg),200

@app.route('/incomes/<zipcode>',methods=['GET','DELETE','PUT','POST'])
def income(zipcode):
  """GET: Returns income data for a specified zip. DELETE: deletes it. PUT: updates it. POST: creates new income data for a zipcode."""
  global agg
  if request.method == 'GET':
    a = [x for x in agg if x['zipcode'] == zipcode]
    return json.dumps(a),200
  elif request.method == 'PUT':
    record = [x for x in agg if x['zipcode'] == zipcode]
    if (len(record) == 0) or (len(record) > 1):
      return 'Error: {} records matched ID {}.'.format(len(record),insp_id),501
    record = record[0]
    b = json.loads(request.get_data())
    print('body: {}'.format(b))
    for k in b:
      record[k] = b[k]
    d = [x for x in agg if x['zipcode'] != zipcode]
    d.append(record)
    agg = d
    return json.dumps({"result":"success"}),200
    pass
  elif request.method == 'POST':
    zs = [x for x in agg if x['zipcode']==zipcode]
    if len(zs) > 0:
      return json.dumps({"result":"error"}),501
    b = json.loads(request.get_data())
    b['zipcode']=zipcode
    agg.append(b)
    return json.dumps({"result":"success"}),200
  else: #request.method == 'DELETE'
    a = [x for x in agg if x['zipcode'] != zipcode]
    if len(a) == len(agg):
      return json.dumps({"result":"error"}),501
    else:
      agg = a
      return json.dumps({"result":"success"}),200


def distance(x1, y1, x2, y2):
  return math.sqrt(math.pow(x1-x2, 2) + math.pow(y1-y2, 2))

@app.route('/100closest/<latlong>')
def closest100(latlong):

  latlong = latlong.translate(dict.fromkeys(map(ord, '()"" '), None))
  latitude_str, longitude_str = latlong.split(',');
  latitude = float(latitude_str)
  longitude = float(longitude_str)

  data.sort(key=lambda item: distance(latitude, longitude, item["Latitude"], item["Longitude"] ) )
  closest100 = data[0:100]

  return json.dumps(closest100)
  #return str ([x['AKA Name'] for x in closest100])

@app.route('/namematch/<s>')
def namematch(s):
  return json.dumps([x for x in data if s in x["DBA Name"]])

@app.route('/failsbyzip/<zip>')
def failsbyzip(zip):
  databyzip = [x for x in data if (x["Zip"] == zip)]
  return json.dumps([x for x in databyzip if (x["Results"] == "Fail")])

@app.route('/makereport')
def makereport():
  names = [("STARBUCKS COFFEE", "starbucks.com"),
           ("CHIPOTLE MEXICAN GRILL", "chipotle.com"),
           ("WHOLE FOODS MARKET", "wholefoodsmarket.com"),
           ("AU BON PAIN", "aubonpain.com"),
           ("SUBWAY", "subway.com"),
           ("7-ELEVEN", "7eleven.com"),
           ("CITGO", "citgo.com"),
           ("WENDY'S", "wendys.com"),
           ("JAMBA JUICE", "jambajuice.com"),
           ("DOMINO'S PIZZA", "dominos.com")]

  company_info = []
  for n in names:
    info = []

    # logo
    info.append("https://logo.clearbit.com/" + n[1])

    # name
    info.append(n[0])

    # number of locations
    inspections = [x for x in data if (x["AKA Name"] == n[0])]
    locations = []
    for i in inspections:
      loc = (i["Latitude"], i["Longitude"])
      if loc not in locations:
        locations.append(loc)

    info.append(str(len(locations)))

    # % inspections failed
    failed = [x for x in inspections if (x["Results"] == "Fail")]
    info.append(str((len(failed) / len(inspections)) * 100))

    # avg length of violation
    avg = 0
    for i in inspections:
      avg += len(i["Violations"])
    avg /= len(inspections)
    info.append(str(avg))

    company_info.append(info)

  return render_template('report.html', company_info=company_info)


#app.run(host='0.0.0.0')
app.run(host='0.0.0.0',debug=True)

