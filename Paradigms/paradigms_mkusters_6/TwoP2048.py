#
# Madeline Kusters
#

import random
import functools
from flask import render_template

#
#


class board:
  """A class to manage a two-player 2048 board."""

  # this embodies the search pattern for empty cells.
  spiral = [[1,1],[1,2],[2,2],[2,1],[2,0],[1,0],[0,0],[0,1],[0,2],[0,3],[1,3],[2,3],[3,3],[3,2],[3,1],[3,0]]

  def __init__(self):
    """initialize the board."""
    self.board = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

  def __str__(self):
    res = ""
    for row in self.board:
      for item in row:
        res += str(item) + " "
      res += "\n"
    res = res[:-1]
    return res

  def set_board(self,b):
    self.board = b.copy()

  def isfull(self):
    """returns False if there's an empty cell."""
    flat = functools.reduce(lambda a,b: a+b, self.board)
    return not 0 in flat

  def computer_update(self):
    empty = self.emptyindex()
    self.place(2, empty)

  def emptyindex(self):
    """return the 2D index of the first empty cell."""
    for entry in self.spiral:
      if self.board[entry[0]][entry[1]] == 0:
        return entry
    return False

  def place(self,n,loc):
    """place an n in the specified location location [r,c]."""
    self.board[loc[0]][loc[1]] = n
    return 0

  def flip(self):
    """flip the board left-to-right."""
    # legit one-liner
    self.board = [x[::-1] for x in self.board]

  def transpose(self):
    """transpose the board."""
    self.board = [list(t) for t in list(zip(*self.board))]

  def move(self, dir):
    if (dir == "left"):
      return self.move_left()
    elif (dir == "right"):
      return self.move_right()
    elif (dir == "up"):
      return self.move_up()
    elif (dir == "down"):
      return self.move_down()
    else:
      #invalid, so we just return a score increase of 0 and the unchanged board
      print("Invalid direction: {}".format(dir))
      return [0, self.board]

  def move_right(self):
    """move right; return [score increase, new_board]. Do not modify this board."""
    s = 0
    nb = []
    for r in self.board:
      r2 = [x for x in r if x != 0] # collapse zeros
      r3 = []
      if len(r2) >= 2:
        while len(r2) > 1:
          if r2[0] == r2[1]:
            r3 = r3 + [2*r2[0]]
            s = s + 2*r2[0]
            r2 = r2[2:]
          else:
            r3 = r3 + [r2[0]]
            r2 = r2[1:]
      if len(r2) == 1:
        r3 = r3 + [r2[0]]
      r3 = [0,0,0,0]+r3
      r3 = r3[-4:]
      nb.append(r3)
    b_out = board()
    b_out.set_board(nb)
    return [s,b_out]

  def move_left(self):
    """move left; return score increase."""
    # rotate board 180 degrees
    self.flip()
    # move_right
    (score, nb) = self.move_right()
    # rotate board back
    self.flip()
    nb.flip()
    return [score, nb]

  def move_up(self):
    """move up; return score increase."""
    self.transpose()
    (score, nb) = self.move_left()
    nb.transpose()
    self.transpose()
    return [score, nb]

  def move_down(self):
    """move down; return score increase."""
    self.transpose()
    (score, nb) = self.move_right()
    nb.transpose()
    self.transpose()
    return [score, nb]

  def expand(self):
    """Expand current state into all possible sucessor states: return list of successor boards."""
    # assume "move_foo" methods return (score, board).
    [s_right,b_right] = self.move_right()
    [s_left,b_left] = self.move_left()
    [s_up,b_up] = self.move_up()
    [s_down,b_down] = self.move_down()

    res = []
    if s_right >= 0: res.append(b_right)
    if s_left >= 0: res.append(b_left)
    if s_up >= 0: res.append(b_up)
    if s_down >= 0: res.append(b_down)

    return res


class player:
  def __init__(self):
    """Initialize a player with a score of 0"""
    self.score = 0

  def make_move(self, direction, board):
    """make a move and update the player's score accordingly, update the board with a new 2 if the board has changed and return the new board"""
    (score_increase, new_board) = board.move(direction)
    self.score += score_increase
    if (new_board.board != board.board):
      # only add a new tile to the board if the move actually changed something
      new_board.computer_update()
    return new_board

class game:
  def __init__(self):
    """Initialize the game with an empty board and 2 players, update the board twice to add those intial 2s"""
    self.board = board()
    self.board.computer_update()
    self.board.computer_update()
    self.player1 = player()
    self.player2 = player()
    self.game_over = False
    self.html = True

  def newgame(self):
    """reinitialize the game."""
    self.__init__()

  def check_for_game_over(self):
    """Check if the game is over, if it is set self.game_over to True """
    if self.board.isfull():
      self.game_over = True
      for b in self.board.expand():
        if b.board != self.board.board:
          self.game_over = False

  def HTML(self):
    """Return a string containing HTML to render the game"""
    #return str(self.board) + " Player 1 score = " + str(self.player1.score) + " Player 2 score = " + str(self.player2.score)
    return render_template('2048.html', board=self.board.board, p1score=self.player1.score, p2score=self.player2.score, game_over=self.game_over)

  def PLAINTEXT(self):
    """Return a plain text string for displaying the game-state in the terminal"""
    res = "2048 Game \n"
    res += str(self.board)
    res += "\n"
    res += "Player 1 score = " + str(self.player1.score) + "\n"
    res += "Player 2 score = " + str(self.player2.score) + "\n"
    if self.game_over:
      res += "\nEND OF GAME\n"
    res += "\n"
    return res

  def toString(self):
    if self.html:
      return self.HTML()
    else:
      return self.PLAINTEXT()




# notes to self
#
# do I need to implement a copy() for the board class? rather than
# assigning boards all over the place
#
# you need to implement a fn to check if a move is valid
