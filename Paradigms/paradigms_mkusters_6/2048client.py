import sys
import TwoP2048

import requests


# get location of server from command line args
if len(sys.argv) == 2:
  serverURL = sys.argv[1]
else:
  serverURL = "http://localhost:5000"


print("2048 Terminal-based Client")
print("use the 'w', 'a', 's', 'd' keys to move up, left, down, and right\n")

r = requests.get(serverURL+"/terminal")



while(1):
  print("Menu (choose one of the following options): ")
  print("1) start 1-player game")
  print("2) start 2-player game")
  print("3) quit")

  choice = input("Enter your choice: ")
  if (choice == "1"):
    num_players = 1
  elif (choice == "2"):
    num_players = 2
  elif (choice == "3"):
    break
  else:
    print("Invalid Choice\n")
    continue


  game_over = False
  r = requests.get(serverURL +"/reset")
  r = requests.get(serverURL+"/terminal")
  r = requests.get(serverURL+"/status")
  print(r.text)

  while not game_over:

    # Player 1's move

    valid = False
    while not valid:
      direction = input("Player 1 move: ")
      if (direction == "w"):
        r = requests.get(serverURL + "/up/1")
        valid = True
      elif (direction == "a"):
        r = requests.get(serverURL + "/left/1")
        valid = True
      elif (direction == "s"):
        r = requests.get(serverURL + "/down/1")
        valid = True
      elif (direction == "d"):
        r = requests.get(serverURL + "/right/1")
        valid = True
      else:
        print("Invalid direction\n")

    print("")
    print(r.text)

    # Check for end of game

    if r.text.find("END OF GAME") != -1:
      game_over = True
      continue

    # if this is a 1 player game or if the game is over, skip player 2's turn

    if (num_players == 1 or game_over):
      continue

    # Players 2's turn

    valid = False
    while not valid:
      direction = input("Player 2 move: ")
      if (direction == "w"):
        r = requests.get(serverURL + "/up/2")
        valid = True
      elif (direction == "a"):
        r = requests.get(serverURL + "/left/2")
        valid = True
      elif (direction == "s"):
        r = requests.get(serverURL + "/down/2")
        valid = True
      elif (direction == "d"):
        r = requests.get(serverURL + "/right/2")
        valid = True
      else:
        print("Invalid direction\n")

    print(r.text)

    # Check for end of game

    if r.text.find("END OF GAME") != -1:
      game_over = True








