My server, 2048server.py, has the following endpoints:

/reset
--Resets to the beginning of a new game
--Returns a string representation of the game
----This is either in HTML or Plain Text, the default is HTML

/terminal
--Sets a flag in the game state that switches the string output to plain text
--Returns a string representation of the game (now in plain text)
--In order to switch back to HTMl, simply go to /reset to start a new game because the default output is in HTML

/status
--Returns string representation of the current game

/testendgame
--Sets the board state to a very full near end-game board in order to quickly test playing at the end game
--Returns string representation of the game

/right/1  (and /left/2, etc)
--Moves player 1 to the right and returns a string representation of the game
--Same for all 4 directions and players 1 and 2

To run the server:
$ python3 2048server.py


My client, 2048client.py, is an interactive text-based terminal
client. It prompts the user for input which should be self-explanatory
when it is run. When the client starts, it first requests the endpoint
/terminal in order to switch the output format from the server to
plain text.

To run the client:
$ python3 2048client.py <server URL>

The <server URL> argument is optional and by default is set to "http://localhostL:5000"
