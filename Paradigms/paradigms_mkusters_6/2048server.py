import sys
import TwoP2048

from flask import Flask
from flask import render_template


app = Flask(__name__)
game = TwoP2048.game()



@app.route('/')
def index():
  str =  "<html> <body> \
<h1> 2048 Game </h1> \
<p> /reset - resets the game </p> \
<p> /status - shows current board </p> \
<p> /[direction]/[player] - makes a move for player (1 or 2) in the direction (left, right, up or down) </p> \
</body> </html>"
  return str

@app.route('/terminal')
def set_plaintext():
  game.html = False
  return game.toString()

@app.route('/reset')
def reset():
  game.newgame()
  return game.toString()

@app.route('/status')
def status():
  return game.toString()

@app.route('/testendgame')
def endgame():
  game.board.board = [[4, 8, 16, 32], [64, 128, 256, 512], [1024, 2048, 4, 32], [0, 8, 16, 512]]
  return game.toString()

@app.route('/<direction>/<player>')
def move(direction, player):
  if (player == "1"):
    new_board = game.player1.make_move(direction, game.board)
    # check here to make sure you got good output
    game.board = new_board
  elif (player == "2"):
    new_board = game.player2.make_move(direction, game.board)
    game.board = new_board
  else:
    return "Error: Invalid player number. Player must be either '1' or '2'"

  game.check_for_game_over()

  return game.toString()


app.run(debug=True)


