## string processing is uber important in python
# in every program flynn finds himself chewing up some strings

# less portable between python 2 and 3

# using python 3.6

s="Four score and seven years ago"
print s
dir(s)

# this is equivalent to..
s.__add__("our forefathers brought forth")

# this
s + "our forefathers brought forth"


# methods that don't start with __ are generally class specific methods

ss="Myxdogxhasxfleas"

ss.split('x')

# formatting -- like sprintf

outstring = "Here is a cute string: {0}".format(s)

print(outstring)  # in python 3 print is a function not a command

def functionname(requiredarg, naedarg=defaultvalue,*args, **kwargs):
  print("requiredarg is {0} ".format(requiredarg))
  print("namedarg is {0}".format(namedarg))
  print("additional args is {0}".format(args))
  for k,v in kwargs.items():
    print("kwargs[{0}] = {1}".format(k,v))

functionname(32)
functionname(32, namedarg={1:2})


#generic function takes any kind of arguments

def f(*args, **kwargs):
  print ("additiona args is {0}".format(args))
  for k.v in kwargs.items():
    print("kwargs[{0}] = {1}".format(k,v))


# now you can call f with an arbitrary number of arguments and named arguments


# supply a function with a doc string
def func(*args, **kwargs):
  """This is a generic argument printing function"""
  #blahc blah blah
  # now when you call func.__doc__() it will print that doc string


# introspection - an object that doesn't know what it is


# serialize an object - turn it into a string serialize -- uses
# introspection to figure out what what kind of object it is being
# asked to serialize

# pickle
import pickle

object.pickle()
# gives you the representation on disk

# classes

class Felidae(object):
  """Base (abstract) class for Felidae"""
  # python allows you to instantiate abstract classes
  def has_fur(self): return True
  def can_roar(self): pass  #creates a slot for the function to be implemented in sub classes
  def has_spots(self): return False


class Pantherinae(Felidae):
  """Base (abstract) class for Pantherinae."""
  def can_roar(self):return True


class Lion(Pantherinae):
  """Class for Lion"""
  species_name = "p.Leo"
  pic_url = ""


class Leopard(Pantherinae):
  """Class for Leopard"""
  species_name = "P.pardus"
  def has_sport(self): return True



class Thing(object):
  def __init__(self, v=29):
    self.v = v
  def __str__(self):
    return "<Thing with v = {0}".format(self.v)
  def __repr__(self):
    return "<Thing with v = {0}".format(self.v)

t1 = Thing(108)
str(t1)


#functional programming in python

def outer():
  def inner(): print("Inside inner")
  return inner

# similar to currying,

outer() # returns the function inner

outer()() # calls inner

# if you need a helper function

# helps you avoid cluttering your namespace

# use case - remember values of functions for previous inputs

# wrap a long running function in a wraper that keeps trakc of past
# calls to the function, in case hte value you want now was previously
# computed


def fib(n):
  if n == 0:
    return 0
  elif n == 1:
    return 1
  else:
    return fib(n-1) + fib(n-2)

def memoize(f):
  memo = {}
  def helper(x):
    if x not in memo:
      memo[x] = f(x)
    return memo[x]
  return helper

fib2 = memoize(fib)
fib2(190)

import sys
sys.getrecursionlimit()
sys.setrecursionlimit(1000000)



# decorator:
@memoize
# this means wrap the following function in the function memoize
# not like a directive, you can supply argument to the @ function
def fib(n):
  if n==0: return 0
  elif n==1: return 1
  else: return fib(n-1) + fib(n-2)


