def call_counter(func):
  def helper(*args, **kwargs):
    helper.calls+=1
    return func(*args, **kwargs)
  helper.calls = 0
  helper.__name__=func.__name__

  return helper


@call_counter
def succ(x):
  return x+1

# using decorators, treating functions like data
