# kaggle website that exists to host competitions in data science and machine learning

# list comprehensions

# do a list based computation in a one-liner

# reccomendation: don't nest them

budgets = [x[9] for x in things]
budgets2 = [b for b in budgets if b != '']
budgets3 = [int(b) for b in budgets2]

def greeter():
  print("hello")

greeter()

def repeat(fn, times):
  for i in range(times):
    fn()

repeat(greeter, 3)

def outer(x):
  def inner():
    print(x)
  return inner


p1 = outer(1)

p1()

p2 = outer(2)

p2()

# inner functions defined at non-global scope, remember what their enclosing namespaces looked like at definition time
