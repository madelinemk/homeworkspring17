;; Madeline Kusters

(load "paradigms_1.scm")
(define testlist1 '(turkey gravy stuffing potatoes ham peas))
(define testlist2 '(bacon turkey beef turkey))
(define testlist3 '(bacon turkey beef))
(define nestedlist '(1 (0 2) 0))

;; replacefirst
(define replacefirst
	(lambda (a b lat)
		(cond ((null? lat) '())
					((equal? a (car lat)) (cons b (cdr lat)) )
					(else (cons (car lat) (replacefirst a b (cdr lat) ) ) ))))

;; replaceevery
(define replaceevery
	(lambda (a b lat)
		(cond ((null? lat) '())
					((equal? a (car lat)) (cons b (replaceevery a b (cdr lat))))
					(else (cons (car lat) (replaceevery a b (cdr lat)))))))


;; tests!
(display (replacefirst 'turkey 'cheese testlist1))
(display "\n")

(display (replaceevery 'turkey 'cheese testlist1))
(display "\n")

(display (replacefirst 'turkey 'bacon testlist2))
(display "\n")

(display (replaceevery 'turkey 'bacon testlist2))
(display "\n")

(display (replacefirst 'sauce 'apple testlist3))
(display "\n")

;; notice that this is equivalent to using testlist3
(display (replacefirst 'beef 'carrots (list 'bacon 'turkey 'beef)))
(display "\n")

(display (replacefirst 0 1 nestedlist) )
(display "\n")
