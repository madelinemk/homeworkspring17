(load "paradigms_1.scm")

;; Madeline Kusters
(define q '(turkey (gravy) (stuffing potatoes ham) peas))




;; question 1
(display "question 1: ")
(display (atom? (car (cdr (cdr q)))))
(display "\n")
;; output: (copy the output you saw here)
;; question 1: #f
;; explanation: (use as many lines as necessary, just add more comments)

;; it checks if the car of the cdr of the cdr of the list q is an atom
;; or not.  Starting at the deepest level of nesting, the cdr of q is
;; ((gravy) (stuffing potatoes ham) peas)).  The cdr of that is
;; ((stuffing potatoes ham) peas)). And finally the car of that is
;; (stuffing potatoes ham).  This is a list and not an atom, so the
;; atom? function returns false.


;; question 2
(display "question 2: ")
(display (lat? (car (cdr (cdr q)))))
(display "\n")
;; output:
;; question 2: #t
;; explanation:

;; The function lat? checks if the argument provided to it is a list
;; of atoms. This question checks if the car of the cdr of the cdr of
;; q is a list of atoms.  The item in question here is the same as in
;; question 1. So, we are looking at (stuffing potatoes ham) to see if
;; it is a list of atoms.  It is, so the function lat? returns true.


;; question 3
(display "question 3: ")
(display (cond ((atom? (car q)) (car q)) (else '())))
(display "\n")
;; output:
;; question 3: turkey
;;
;; explanation:

;; This line of code displays the car of q is the car of q is an atom,
;; otherwise it displays and empty list. The car of q is turkey, so
;; turkey is diplayed.
