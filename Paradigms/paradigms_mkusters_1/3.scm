;; Madeline Kusters

(load "paradigms_1.scm")

(define blankcell?
	(lambda (lat)
		(cond ((null? lat) #f)
					((equal? #f (car lat)) #t)
					(else (blankcell? (cdr lat))))))

;; tests

(display (blankcell? '(16 32 #f 256)))
(display "\n")
(display (blankcell? '(2 2 8 1024)))
(display "\n")
(display (blankcell? '((#f 2 2 2))))
(display "\n")
(display (blankcell? '()))
(display "\n")
