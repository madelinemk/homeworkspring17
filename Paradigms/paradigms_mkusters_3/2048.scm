;; Madeline Kusters
;; Paradigms Homework 3


(load "paradigms_lib.scm")

;;
;;Problem 1
;;

(define blankcell?
	(lambda (lat)
		(cond ((null? lat) #f)
					((equal? #f (car lat)) #t)
					(else (blankcell? (cdr lat))))))

(define blankcell?_wrapper
	(lambda (lat)
		(cond ((not (equal? 4 (list-size lat))) (throw 'board-layout-exception))
					((not (lat? lat)) (throw 'board-layout-exception))
					(else (blankcell? lat)))))
;;note, this only checks if it is a list of atoms with length 4, it
;;does NOT ensure that the atoms in the list are only constant
;;integers or #f


;;tests
(define test1 '(2 2 2 4))
(define test2 '(2 #f #f 4))

(display (blankcell?_wrapper test1))
(display "\n")

(display (blankcell?_wrapper test2))
(display "\n")


;;
;;Problem 2
;;

(define remove-falses
	(lambda (l)
		(cond ((null? l) '())
					((equal? #f (car l)) (remove-falses (cdr l)))
					(else (cons (car l) (remove-falses (cdr l)))))))

(define add-falses
	(lambda (l)
		(cond ((equal? 4 (list-size l)) l)
					(else (add-falses (cons #f l))))))

;; Adding reverse-row because my previous implementation of
;; combine-numbers was greedy on the left, instead of the right. I
;; thought this was the easiest way to switch my implementation of
;; move-right-row to being greedy on the right
(define reverse-row
	(lambda (l)
		(define reverse-helper
			(lambda (l acc)
				(cond
				 ((null? l) acc)
				 (else (reverse-helper (cdr l) (cons (car l) acc))))))
	 (reverse-helper l '() )))

(define combine-numbers
	(lambda (l)
		(cond ((null? l) '())
					((equal? 1 (list-size l)) l)
					((equal? (car l) (car (cdr l))) (cons (* 2 (car l)) (combine-numbers (cdr (cdr l)))))
					(else (cons (car l) (combine-numbers (cdr l)))))))

(define move-right-row
	(lambda (l)
		(add-falses (reverse-row (combine-numbers (reverse-row (remove-falses l)))))))

;; tests
(display "Tests for move-right-row:\n")
(display (move-right-row '(#f 2 2 2)))
(display "\n")

(display (move-right-row '(2 2 4 4)))
(display "\n")

(display (move-right-row '(#f #f #f #f)))
(display "\n")


(define move-right
	(lambda (board)
		(cond ((null? board) '())
					(else (cons (move-right-row (car board)) (move-right (cdr board)))))))

;;tests
(display "\nTest for move-right:\n")
(define test-board '( (#f 2 2 2) (2 2 4 4) (#f #f #f #f) (256 256 512 1024)))
(display (move-right test-board))
(display "\n\n")

;;
;; Problem 3
;;

;; For this I can use reverse-row that I wrote for the last problem


(define vflip
	(lambda (board)
		(cond ((null? board) '() )
					(else (cons (reverse-row (car board)) (vflip (cdr board)))))))

;;test
(display "\nTest for vflip: \n")
(display "Original:  ")
(display test-board)
(display "\nFlipped:   ")
(display (vflip test-board))
(display "\n")

;;
;; Problem 4
;;

(define remove-1st-col
	(lambda (board)
		(cond ((null? board) '() )
					(else (cons (cdr (car board)) (remove-1st-col (cdr board)))))))

(define gen-row
	(lambda (board)
		(cond ((null? board) '() )
					(else (cons (car (car board)) (gen-row (cdr board)))))))


(define transpose
	(lambda (board)
		;; Return a matrix whose columns are the rows of the original
		(cond ((null? (car board)) '())
					(else (cons (gen-row board) (transpose (remove-1st-col board)))))))

;; Test
(display "\nTest for transpose: \n")
(display "Original:  ")
(display test-board)
(display "\nTransposed:   ")
(display (transpose test-board))
(display "\n")


;;
;; Problem 5
;;


(define move-left
	(lambda (board)
		(vflip (move-right (vflip board)))))

(define move-up
	(lambda (board)
		(transpose (vflip (move-right (vflip (transpose board)))))))

(define move-down
	(lambda (board)
		(transpose (vflip ( move-left ( vflip (transpose board)))))))

;; Tests
(define left '((#f 2 2 2) (#f #f #f #f)  (#f #f #f #f) (#f #f #f #f)))
(display "\nTest for move-left: \n")
(display "Original:  ")
(display left)
(display "\nMoved Left:   ")
(display (move-left left))
(display "\n")

(define up '((2 2 2 2) (2 #f #f #f)  (2 #f #f #f) (#f #f #f #f)))
(display "\nTest for move-up: \n")
(display "Original:  ")
(display up)
(display "\nMoved Up:   ")
(display (move-up up))
(display "\n")

(define down '((2 2 2 2) (2 #f #f #f)  (2 #f #f #f) (#f #f #f #f)))
(display "\nTest for move-down: \n")
(display "Original:  ")
(display down)
(display "\nMoved Down:   ")
(display (move-down down))
(display "\n\n")

