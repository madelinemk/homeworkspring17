(define-public (atom? x)
	(and (not (null? x))
			 (not (pair? x))))

(define-public lat?
	(lambda (l)
		(cond
		 ((null? l) #t)
		 ((atom? (car l)) (lat? (cdr l)))
		 (else #f))))

(define-public list-size
	(lambda (l)
		(cond ((null? l) 0)
					(else (+ 1 (list-size (cdr l)))))))
