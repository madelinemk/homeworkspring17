# I chose these import statements just to make my code look cleaner
# For this to work you would just need to import cocos and then add the subdirectory after
# Ex: self.sprite = Sprite('directory') would be self.sprite = cocos.sprite.Sprite('directory')
from __future__ import print_function
import urllib

import cocos
from cocos.layer import Layer, ColorLayer
from cocos import layer
from cocos.sprite import Sprite
from cocos.actions import *
from cocos.director import director
from time import sleep


SPEED = 100
def time_duration(distance, speed):
	"""Calculate the time it would take to travel a given distance at a given speed.  
	   0.001 is the minimum, because I always need the sprite to take some amount of 
	   time to move, otherwise it would just Jump to the next location """
	if (float(distance)/speed > 0):
		return float(distance)/speed
	else:
		return 0.001

# Here I make a class that extends cocos' ColorLayer class
# This type of layer is different because it has a background color! (awesome right?)
class Actions(ColorLayer):
	is_event_handler = True;

	def __init__(self):
		super(Actions, self).__init__(52, 152, 219, 1000)
		self.sprite = Sprite('grossini.png')
		print("window = {} x {}, sprite = {} x {}".format(self.width, self.height, self.sprite.width, self.sprite.height))
		print(dir(self.sprite))

		# set starting position to top left corner 
		self.sprite.position = 0+self.sprite.height/2, self.height-self.sprite.width/2
		# set starting orientation so the head is facing the top right corner
		self.sprite.rotation = 90

		self.add(self.sprite)
		self.move()
	
	
	def move(self):
		global SPEED
		r = RotateBy(90.0, time_duration(90, SPEED))
		
		# the width and height to be actually traveled by the sprite 
		#(the height of the sprite is excluded because the entire body needs to always be within the window)
		w = self.width - self.sprite.height
		h = self.height - self.sprite.height

		# get current position of sprite
		sprite_x = self.sprite.position[0]
		sprite_y = self.sprite.position[1]
		
		# create actions for moving the length of each side 
		move_top = MoveBy((w, 0), time_duration(w, SPEED))
		move_right_side = MoveBy((0, -h), time_duration(h, SPEED))
		move_bottom = MoveBy((-w, 0), time_duration(w, SPEED))
		move_left_side = MoveBy((0, h), time_duration(h, SPEED))
		
		# default m -- the sequence of moves if starting from the top left corner
		m = move_top + r + move_right_side + r + move_bottom + r + move_left_side + r
		
		# Check the location of the sprite and create the pattern of actions assigned to m accordingly
		if (sprite_y > self.height-self.sprite.width):
			if (sprite_x < self.sprite.height):
				print("top left corner")
				# make sure sprite is the correct rotation and flush with the corner
				self.sprite.rotation = 90
				self.sprite.position = self.sprite.height/2, self.height-self.sprite.width/2
				m = move_top + r + move_right_side + r + move_bottom + r + move_left_side + r
			elif (sprite_x > w):
				print("top right corner")
				# make sure sprite is the correct roation and flush with the corner
				self.sprite.rotation = 180
				self.sprite.position = self.width-self.sprite.height/2, self.height-self.sprite.width/2
				m = move_right_side + r + move_bottom + r + move_left_side + r + move_top + r
			else:
				print("top")
				# calculate the distances to travel on either side of the sprite's current position
				# chunk1 is the section of the current side that is in front of the sprite
				# chunk2 is the section of the current side that is behind the sprite
				chunk1 = self.width-self.sprite.height/2-sprite_x
				chunk2 = sprite_x-self.sprite.height/2
				m = MoveBy((chunk1, 0), time_duration(chunk1, SPEED)) +\
				    r + move_right_side + r + move_bottom + r + move_left_side + r +\
				    MoveBy((chunk2, 0), time_duration(chunk2, SPEED))

		elif (sprite_y < self.sprite.width):
			if (sprite_x < self.sprite.height):
				print("bottom left corner")
				# make sure sprite is the correct rotation and flush with the corner
				self.sprite.rotation = 0
				self.sprite.position = self.sprite.height/2, self.sprite.width/2
				m = move_left_side + r + move_top + r + move_right_side + r + move_bottom + r
			elif (sprite_x > w):
				print("bottom right corner")
				# make sure sprite is the correct rotation and flush with the corner
				self.sprite.rotation = 270
				self.sprite.position = self.width-self.sprite.height/2, self.sprite.width/2
				m = move_bottom + r + move_left_side + r + move_top + r + move_right_side + r
			else:
				print("bottom")
				# calculate the distances to travel on either side of the sprite's current position
				# chunk1 is the section of the current side that is in front of the sprite
				# chunk2 is the section of the current side that is behind the sprite
				chunk1 = sprite_x - self.sprite.height/2
				chunk2 = self.width - self.sprite.height/2 - sprite_x
				m = MoveBy((-chunk1, 0), time_duration(chunk1, SPEED)) +\
				    r + move_left_side + r + move_top + r + move_right_side + r +\
				    MoveBy((-chunk2, 0), time_duration(chunk2, SPEED))
		elif (sprite_x < self.sprite.width):
			print("left side")
			# calculate the distances to travel on either side of the sprite's current position
			# chunk1 is the section of the current side that is in front of the sprite
			# chunk2 is the section of the current side that is behind the sprite
			chunk1 = self.height - self.sprite.height/2 - sprite_y
			chunk2 = sprite_y - self.sprite.height/2
			m = MoveBy((0, chunk1), time_duration(chunk1, SPEED)) +\
			    r + move_top + r + move_right_side + r + move_bottom + r +\
			    MoveBy((0, chunk2), time_duration(chunk2, SPEED)) 
			
		elif (sprite_x > self.width - self.sprite.width):
			print("right side")
			# calculate the distances to travel on either side of the sprite's current position
			# chunk1 is the section of the current side that is in front of the sprite
			# chunk2 is the section of the current side that is behind the sprite
			chunk1 = sprite_y - self.sprite.height/2
			chunk2 = self.height - self.sprite.height/2 - sprite_y
			m = MoveBy((0, -chunk1), time_duration(chunk1, SPEED)) +\
			    r + move_bottom + r + move_left_side + r + move_top + r +\
			    MoveBy((0, -chunk2), time_duration(chunk2, SPEED)) 
		else:
			# this should never happen, but I will leave it in
			# reset the sprite position to the top left corner and perform the default action pattern 
			print("somehow the sprite is not in any of the 4 edges, resetting position to top-left corner")
			self.sprite.rotation = 90
			self.sprite.position = 0+self.sprite.height/2, self.height-self.sprite.width/2

		# tell the sprite to action do the sequence of actions, m, that we have been building
		# and have it Repeat that sequence indefinitely
		self.sprite.do(Repeat(m))


	def on_key_press(self,key,modifiers):
		global SPEED
		k = chr(key)
		if (k == '+' or k == '='): 
			print("increasing speed")
			SPEED = SPEED * 2
		elif k == '-': 
			print("decreasing speed")
			SPEED = SPEED / 2
		else: 
			print("key pressed: {}".format(k))
		#print("SPEED = {}".format(SPEED))

		print("removing first action")
		self.sprite.remove_action(self.sprite.actions[0])
		print("calling move")
		self.move()
	


director.init()
director.run(cocos.scene.Scene(Actions()))

