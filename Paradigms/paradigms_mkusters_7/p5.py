# Madeline Kusters

import requests
import requests_cache
import gmplot

requests_cache.install_cache("demo_cache")

#
# map markers income tax percentage
# green markers are the lowest tax percentage
# blue markers are the second quartile
# yellow markers are the third quartile
# red markers are the fourth quartile and highest tax percentage
#


#
# get income data
#
raw_income_data = []
r = requests.get("http://73.15.3.74:5000/incomes")
if (r.status_code == 200):
    raw_income_data = r.json()
else:
    print("Warning: did not recieve income data. Status code {}".format(r.status_code))

#
# get zipcodes from food data
#
food_zips = []
r = requests.get("http://73.15.3.74:5000/zips")
if (r.status_code == 200):
    food_zips = r.json()
else:
    print("Warning:did not recieve zipcodes from food inspection data. Status Code {}".format(r.status_code))


#
# remove records in the income_data list that do not have a corresponding zip
# code in the food inspection data
#
income_data = []
for record in raw_income_data:
    if record["zipcode"] in food_zips:
        income_data.append(record)

markers = []
for record in income_data:
    # create marker dictionary for this record
    marker = {}
    marker["zipcode"] = record["zipcode"]
    marker["tax_rate"] = record["A06500"] / record["A00100"]

    markers.append(marker)


#
# calculate latitude/longitude for each marker
#
for marker in markers:
    r = requests.get("http://73.15.3.74:5000/inspectionszip/"+marker["zipcode"])

    inspections = []
    if (r.status_code == 200):
        #print("Request successful")
        inspections = r.json()
    else:
        print("Warning: failed to get inspectionszip data. Status Code {}".format(r.status_code))

    if (len(inspections) == 0):
        print("Warning there is no food inspection data for zipcode {}, skipping this marker".format(marker["zipcode"]))
        continue

    avg_lat  = 0
    avg_long = 0
    for record in inspections:
        avg_lat  += record["Latitude"]
        avg_long += record["Longitude"]
    avg_lat  /= len(inspections)
    avg_long /= len(inspections)

    marker["latitude"] = avg_lat
    marker["longitude"] = avg_long


#
# sort markers based on average income and set color based on quartile
#
markers.sort(key=lambda marker: marker["tax_rate"])
length = len(markers)


for marker in markers[0:int(length/4)]:
    marker["color"] = "#00FF00" #green

for marker in markers[int(length/4):int(length/2)]:
    marker["color"] = "#0000FF" #blue

for marker in markers[int(length/2):int(length*3/4)]:
    marker["color"] = "#FFFF00" #yellow

for marker in markers[int(length*3/4):]:
    marker["color"] = "#FF0000" #red


#
# Calculate overall center latitude and longitude for initializing the map
#
lat_center = 0
long_center = 0
for marker in markers:
    lat_center += marker["latitude"]
    long_center += marker["longitude"]
lat_center /= len(markers)
long_center /= len(markers)


#print(type(markers[0]["latitude"]))
gmap = gmplot.GoogleMapPlotter(lat_center, long_center, 11)
for marker in markers:
    gmap.marker(marker["latitude"], marker["longitude"], marker["color"])

gmap.draw("p5.html")
