# Madeline Kusters

import requests
import requests_cache
import string
import operator

requests_cache.install_cache("demo_cache")

#
# get text from the violations entry for each zipcode in the inspection reports
#

#print("Getting zips...")
r = requests.get("http://73.15.3.74:5000/zips")
zips = r.json()

#
# get list of stop words
#

#print("Getting stopwords...")
stop_file = open("stopwords.txt", 'r')
stop_words = stop_file.readlines()
stop_words = stop_words[0].split(" ")
stop_file.close()

#
# open outfile
#

outfile = open("p4.txt",'w')

#
# calculate word frequencies for each zipcode
#

for zipcode in zips:
  #print("processing zipcode {} ...".format(zipcode))
  r = requests.get("http://73.15.3.74:5000/inspectionszip/"+zipcode)
  inspection_data = r.json()
  words = []
  for record in inspection_data:
    words += record["Violations"].split()

  #convert to lower case and remove punctuation
  for i in range(len(words)):
    words[i] = words[i].lower()
    words[i] = ''.join(c for c in words[i] if c not in string.punctuation)

  #remove stopwords and words containing only digits
  words[:] = [w for w in words if ((w not in stop_words) or w.isdigit())]

  #calculate frequencies
  total = len(words)
  word_freq = {}
  for w in words:
    if w not in word_freq:
      word_freq[w] = 1
    else:
      word_freq[w] += 1

  for key in word_freq:
    word_freq[key] /= total

  #sort
  sorted_word_freq = sorted(word_freq.items(), key=operator.itemgetter(1))
  sorted_word_freq = sorted_word_freq[::-1]

  #append the zipcode and the top ten to p4.txt
  #print("Printing result to outfile")
  print(zipcode, file=outfile)
  for freq in sorted_word_freq[0:10]:
    print("{}:  {}%".format(freq[0], freq[1] * 100), file=outfile)
  print("\n", file=outfile)

outfile.close()
