# Madeline Kusters

import requests
import requests_cache
import string
import operator

requests_cache.install_cache("demo_cache")

#
# get text from the violations entry of all inspection reports
#

r = requests.get("http://73.15.3.74:5000/zips")
zips = r.json()

violation_words = []
for zipcode in zips:
  r = requests.get("http://73.15.3.74:5000/inspectionszip/"+zipcode)
  inspection_data = r.json()
  for record in inspection_data:
    words = record["Violations"]
    word_list = words.split()
    violation_words += word_list

# print(violation_words)

#
# Remove non-alpha characters and convert to lower case
#

#print(violation_words[:10])

for i in range(len(violation_words)):
  violation_words[i] = violation_words[i].lower()
  violation_words[i] = ''.join(c for c in violation_words[i] if c not in string.punctuation)

#print(len(violation_words))


#
# remove stop words and empty strings and strings containing only digits
#

stop_file = open("stopwords.txt", 'r')
stop_words = stop_file.readlines()
stop_words = stop_words[0].split(" ")
stop_file.close()

violation_words[:] = [word for word in violation_words if ((word not in stop_words) or word.isdigit())]
#print(len(violation_words))
#print(len(stop_words))

#
# count number of occurrences of each distinct word, divide by total number of words to get a fraction
#

total = len(violation_words)

violation_word_freq = {}

for word in violation_words:
  if word not in violation_word_freq:
    violation_word_freq[word] = 1
  else:
    violation_word_freq[word] += 1

for key in violation_word_freq:
  violation_word_freq[key] /= total

#print(len(violation_word_freq))

#
# sort in descending order
#

sorted_violation_word_freq = sorted(violation_word_freq.items(), key=operator.itemgetter(1))
sorted_violation_word_freq = sorted_violation_word_freq[::-1]

#
# output the word and percentages of the top ten
#

outfile = open("p3.txt", 'w')
for freq in sorted_violation_word_freq[0:10]:
  print("{}:  {}%".format(freq[0], freq[1] * 100), file=outfile)
outfile.close()
