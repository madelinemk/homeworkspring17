function zipsearch(zip)
{
    if (zip == "") {
	return;
    }
    var url = "http://localhost:5000/inspectionszip/" + zip;
    
    cont = document.getElementById("content");

    var xmlHTTP = new XMLHttpRequest();
    xmlHTTP.open("GET", url, false);
    xmlHTTP.send(null);
    var stuff = JSON.parse(xmlHTTP.responseText);
    var thing = stuff[0];
    console.log(thing);
    cont.innerHTML = thing.Violations;

    document.getElementById("ID").innerHTML = thing["Inspection ID"];
    document.getElementById("name").innerHTML = thing["DBA Name"];
    document.getElementById("address").innerHTML = thing["Address"];
    document.getElementById("zip").innerHTML = thing["Zip"];
}

function namesearch(name)
{
    if ( name == "") {
	return;
    }
    var url = "http://localhost:5000/namesearch/" + name;
    console.log(url);
    cont = document.getElementById("content");

    var xmlHTTP = new XMLHttpRequest();
    xmlHTTP.open("GET", url, false);
    xmlHTTP.send(null);
    var stuff = JSON.parse(xmlHTTP.responseText);
    var thing = stuff[0];
    cont.innerHTML = thing.Violations;

    document.getElementById("ID").innerHTML = thing["Inspection ID"];
    document.getElementById("name").innerHTML = thing["DBA Name"];
    document.getElementById("address").innerHTML = thing["Address"];
    document.getElementById("zip").innerHTML = thing["Zip"];
}

function reportsearch(text)
{
    if (text == "") {
	return;
    }
    var url = "http://localhost:5000/reportsearch/" + text;
    
    cont = document.getElementById("content");

    var xmlHTTP = new XMLHttpRequest();
    xmlHTTP.open("GET", url, false);
    xmlHTTP.send(null);
    var stuff = JSON.parse(xmlHTTP.responseText);
    var thing = stuff[0];
    cont.innerHTML = thing.Violations;

    document.getElementById("ID").innerHTML = thing["Inspection ID"];
    document.getElementById("name").innerHTML = thing["DBA Name"];
    document.getElementById("address").innerHTML = thing["Address"];
    document.getElementById("zip").innerHTML = thing["Zip"];
}

