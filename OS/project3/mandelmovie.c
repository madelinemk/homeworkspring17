// Madeline Kusters
// OS Project 3
// mandelmovie


#define MIN 0.000001
#define MAX 2
#define NUM_FRAMES 50

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#include <math.h>

int wait_wrapper(int pid) {

  int exit_status;

  if (pid == 0) {
    pid = wait(&exit_status);
  } else {
    pid = waitpid(pid, &exit_status, 0);
  }

  if (pid == -1 && errno == ECHILD) {
    //printf("myshell: no processes left\n");
    return -1;
  } else if (pid == -1) {
    printf("mandelmovie: wait failed: %s\n", strerror(errno));
    exit(1);
  }

  if (WIFEXITED(exit_status)) {
    // printf("myshell: process %d exited normally with status %d\n", pid, WEXITSTATUS(exit_status));
    return 0;
  } else if (WIFSIGNALED(exit_status)) {
    printf("mandelmovie: process %d exited abnormally with signal %d: %s\n", pid, WTERMSIG(exit_status), strsignal(WTERMSIG(exit_status)));
    exit(1);
  } else {
    //this should not happen
    printf("mandelmovie: process %d exited with status %d\n", pid, exit_status);
    exit(1);
  }
}

int main(int argc, char * argv[]) {

  // get command line args
  if (argc < 2) {
    fprintf(stderr, "mandelmovie: expects 1 argument for the number of processes\n");
    exit(1);
  }
  int num_processes = atoi(argv[1]);

  // set up args for mandel with initial values
  char * mandel_args[16];

  char * max_iterations = "2000";
  char * x_coord = "0";
  char * y_coord = "0.636";
  char scale[100];
  char * img_width = "800";
  char * img_height = "800";
  char img_name[100];

  //
  // initialize counting variables
  //
  // current_frame is the image number, like mandelX.bmp
  // current_scale is the value passed to mandel with the -s flag
  int current_frame = 0;
  double current_scale = MAX;
  int completed_mandel_processes = 0;
  int running_mandel_processes = 0;


  int pid;
  while (current_frame < NUM_FRAMES) {

    // fork a new process
    //    printf("Forking a new process.. the number of running processes is:%d  the number of completed processes is:%d\n", running_mandel_processes, completed_mandel_processes);
    current_frame++;
    current_scale *= exp(log(MIN/MAX)/50);
    pid = fork();

    if (pid < 0) {
      //error
      printf("mandelmovie: Fork Failed\n");
      exit(1);
    } else if (pid == 0) {
      //child

      sprintf(scale, "%lf", current_scale);
      sprintf(img_name, "mandel%d.bmp", current_frame);

      //      printf("current_scale= %lf, current_frame= %d\n", current_scale, current_frame);
      //      printf("Calling mandel -m %s -x %s -y %s -s %s -W %s -H %s -o %s\n", max_iterations, x_coord, y_coord, scale, img_width, img_height, img_name);


      mandel_args[0] = strdup("./mandel");
      mandel_args[1] = strdup("-m");
      mandel_args[2] = strdup(max_iterations);
      mandel_args[3] = strdup("-x");
      mandel_args[4] = strdup(x_coord);
      mandel_args[5] = strdup("-y");
      mandel_args[6] = strdup(y_coord);
      mandel_args[7] = strdup("-s");
      mandel_args[8] = strdup(scale);
      mandel_args[9] = strdup("-W");
      mandel_args[10] = strdup(img_width);
      mandel_args[11] = strdup("-H");
      mandel_args[12] = strdup(img_height);
      mandel_args[13] = strdup("-o");
      mandel_args[14] = strdup(img_name);
      mandel_args[15] = NULL;

      execvp(mandel_args[0], mandel_args);


    } else {
      //parent
      running_mandel_processes++;

      if (running_mandel_processes < num_processes) {
        //this means we want to fork again
        continue;
      } else {
        //wait for a process to complete before continuing
        wait_wrapper(0);
        running_mandel_processes--;
        completed_mandel_processes++;
      }
    }
  }


  while(wait_wrapper(0) == 0) {
    running_mandel_processes--;
    completed_mandel_processes++;
  }


  //  printf("Done... the number of completed processes is %d\n", completed_mandel_processes);
  return 0;
}
