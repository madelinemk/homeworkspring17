#include "bitmap.h"

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

struct thread_args {
  struct bitmap *bm;
  double xmin;
  double xmax;
  double ymin;
  double ymax;
  int max;
  int threads;
  int current_thread;
  pthread_t thread_id;
};
struct thread_args * thread_args_create(struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max, int threads, int current_thread);
void thread_args_print(struct thread_args * a);

int iteration_to_color( int i, int max );
int iterations_at_point( double x, double y, int max );
//void compute_image( struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max );
void * compute_image( void * v_args);

void show_help()
{
  printf("Use: mandel [options]\n");
  printf("Where options are:\n");
  printf("-m <max>     The maximum number of iterations per point. (default=1000)\n");
  printf("-x <coord>   X coordinate of image center point. (default=0)\n");
  printf("-y <coord>   Y coordinate of image center point. (default=0)\n");
  printf("-s <scale>   Scale of the image in Mandlebrot coordinates. (default=4)\n");
  printf("-W <pixels>  Width of the image in pixels. (default=500)\n");
  printf("-H <pixels>  Height of the image in pixels. (default=500)\n");
  printf("-o <file>    Set output file. (default=mandel.bmp)\n");
  printf("-n <threads> The number of threads used to compute the image. (default=1)\n");
  printf("-h           Show this help text.\n");
  printf("\nSome examples are:\n");
  printf("mandel -x -0.5 -y -0.5 -s 0.2\n");
  printf("mandel -x -.38 -y -.665 -s .05 -m 100\n");
  printf("mandel -x 0.286932 -y 0.014287 -s .0005 -m 1000\n\n");
}

int main( int argc, char *argv[] )
{
  char c;

  // These are the default configuration values used
  // if no command line arguments are given.

  const char *outfile = "mandel.bmp";
  double xcenter = 0;
  double ycenter = 0;
  double scale = 4;
  int    image_width = 500;
  int    image_height = 500;
  int    max = 1000;
  int    num_threads = 1;

  // For each command line argument given,
  // override the appropriate configuration value.

  while((c = getopt(argc,argv,"x:y:s:W:H:m:o:n:h"))!=-1) {
    switch(c) {
    case 'x':
      xcenter = atof(optarg);
      break;
    case 'y':
      ycenter = atof(optarg);
      break;
    case 's':
      scale = atof(optarg);
      break;
    case 'W':
      image_width = atoi(optarg);
      break;
    case 'H':
      image_height = atoi(optarg);
      break;
    case 'm':
      max = atoi(optarg);
      break;
    case 'o':
      outfile = optarg;
      break;
    case 'n':
      num_threads = atoi(optarg);
      break;
    case 'h':
      show_help();
      exit(1);
      break;
    }
  }

  // Display the configuration of the image.
  printf("mandel: x=%lf y=%lf scale=%lf max=%d outfile=%s\n",xcenter,ycenter,scale,max,outfile);

  // Create a bitmap of the appropriate size.
  struct bitmap *bm = bitmap_create(image_width,image_height);

  // Fill it with a dark blue, for debugging
  bitmap_reset(bm,MAKE_RGBA(0,0,255,0));

  // Compute the Mandelbrot image

  // create the structs to hold arguments for each thread
  //  struct thread_args * args_array[num_threads];
  struct thread_args ** args_array = malloc(num_threads * sizeof(struct thread_args *));
  if (!args_array) {
    printf("mandel: Error could not malloc\n");
    exit(1);
  }

  int i;
  for (i = 0; i < num_threads; i++) {
    // *** maybe use 1 based indexing here...
    args_array[i] = thread_args_create(bm, xcenter-scale, xcenter+scale, ycenter-scale, ycenter+scale, max, num_threads, i);
  }

  /*
  for (i = 0; i < num_threads; i++) {
    thread_args_print(args_array[i]);
  }
  */

  // create N threads with pthread_create
  int return_stat;
  for(i = 0; i < num_threads; i++) {
    return_stat = pthread_create(&(args_array[i]->thread_id), NULL, &compute_image, args_array[i]);
    //    compute_image(args_array[i]);

    if (return_stat != 0) {
      perror("mandel: pthread_create");
      exit(1);
    }
  }

  // join threads
  void * res;
  for (i = 0; i < num_threads; i++) {
    return_stat = pthread_join(args_array[i]->thread_id, &res);
    if (return_stat != 0) {
      perror("mandel: pthread_join");
      exit(1);
    }
    free(res);
  }

  // Save the image in the stated file.
  if(!bitmap_save(bm,outfile)) {
    fprintf(stderr,"mandel: couldn't write to %s: %s\n",outfile,strerror(errno));
    return 1;
  }

  return 0;
}

/*
  Compute an entire Mandelbrot image, writing each point to the given bitmap.
  Scale the image to the range (xmin-xmax,ymin-ymax), limiting iterations to "max"
*/


void * compute_image( void * v_args)
{

  struct thread_args * args = (struct thread_args *) v_args;

  struct bitmap *bm = args->bm;
  double xmin = args->xmin;
  double xmax = args->xmax;
  double ymin = args->ymin;
  double ymax = args->ymax;
  int max = args->max;

  int i,j;

  int width = bitmap_width(bm);
  int height = bitmap_height(bm);

  // For every pixel in the image...

  // instead of doing j = 0 to height, calculate the slice that will be computed
  int slice_size = (double) height / args->threads;
  int slice_min = slice_size * args->current_thread;
  int slice_max = slice_min + slice_size;

  // if ( slice_max > (height - slice_size) && slice_max < height ) {
  //  slice_max = height;
  //}

  if (args->current_thread == (args->threads-1)) {
    slice_max = height;
  }

  //  printf("I am thread %d calculating rows %d through %d\n", args->current_thread, slice_min, slice_max);

  for(j=slice_min;j<slice_max;j++) {

    for(i=0;i<width;i++) {

      // Determine the point in x,y space for that pixel.
      double x = xmin + i*(xmax-xmin)/width;
      double y = ymin + j*(ymax-ymin)/height;

      // Compute the iterations at that point.
      int iters = iterations_at_point(x,y,max);

      // Set the pixel in the bitmap.
      bitmap_set(bm,i,j,iters);
    }
  }

  return 0;
}

/*
  Return the number of iterations at point x, y
  in the Mandelbrot space, up to a maximum of max.
*/

int iterations_at_point( double x, double y, int max )
{
  double x0 = x;
  double y0 = y;

  int iter = 0;

  while( (x*x + y*y <= 4) && iter < max ) {

    double xt = x*x - y*y + x0;
    double yt = 2*x*y + y0;

    x = xt;
    y = yt;

    iter++;
  }

  return iteration_to_color(iter,max);
}

/*
  Convert a iteration number to an RGBA color.
  Here, we just scale to gray with a maximum of imax.
  Modify this function to make more interesting colors.
*/

int iteration_to_color( int i, int max )
{
  int gray = 255*i/max;
  return MAKE_RGBA(gray,gray,gray,0);
}


/*
  Create method for thread_args
  Allocate new memory and return a pointer to the new struct
*/

struct thread_args * thread_args_create(struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max, int threads, int current_thread ) {
  struct thread_args * a = malloc(sizeof(*a));
  if (!a) {
    printf("mandel: Error, could not malloc\n");
    exit(1);
  }
  a->bm = bm;
  a->xmin = xmin;
  a->xmax = xmax;
  a->ymin = ymin;
  a->ymax = ymax;
  a->max = max;
  a->threads = threads;
  a->current_thread = current_thread;
  return a;
}

void thread_args_print(struct thread_args * a) {

  printf("*bm: %p, xmin: %lf, xmax: %lf, ymin: %lf, ymax: %lf, max: %d, threads: %d, current_thread: %d, thread_id: %lu\n", a->bm, a->xmin, a->xmax, a->ymin, a->ymax, a->max, a->threads, a->current_thread, a->thread_id);

}
