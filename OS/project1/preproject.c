#include <stdio.h>
#include <signal.h>

char bKeepLooping = 1;

void INTHandler(int signal) {
  bKeepLooping = 0;
}


int main( int argc, char *argv[] ) {

  signal(SIGINT, INTHandler);

  while(bKeepLooping) {

  }

  printf("Exited successfully\n");
  return 0;
}
