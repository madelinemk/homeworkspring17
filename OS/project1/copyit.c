#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

void ALRMHandler(int sig) {
  fprintf(stderr, "copyit: still copying...\n");
}


int main (int argc, char * argv[]) {

  // Set signal handlers
  signal(SIGALRM, ALRMHandler);
  alarm(1);

  // Check for correct number of arguments
  if (argc < 3) {
    fprintf(stderr, "copyit: Too few arguments! \nusage: copyit <sourcefile> <targetfile>\n");
    exit(1);
  } else if (argc > 3) {
    fprintf(stderr, "copyit: Too many arguments! \nusage: copyit <sourcefile> <targetfile>\n");
    exit(1);
  }

  // Print out arguments - for debugging fun
  printf("%s %s %s\n", argv[0], argv[1], argv[2]);
  char * source = argv[1];
  char * target = argv[2];

  //Open the source file
  int source_fd = open(source, O_RDONLY);

  //Catch possible errors from opening the file
  if (source_fd == -1) {
    fprintf(stderr, "copyit: Couldn't open file %s: %s.\n", source, strerror(errno));
    exit(1);
  }

  // Get the size of source file
  struct stat source_info;
  int fstat_return = fstat(source_fd, &source_info);
  if (fstat_return == -1) {
    fprintf(stderr, "copyit: Couldn't fstat file %s: %s.\n", source, strerror(errno));
    exit(1);
  }

  int filesize = source_info.st_size;
  int blocksize = source_info.st_blksize;
  //Print out filesize and block size - for debugging fun
  fprintf(stderr, "Size of source file: %d\n", filesize);
  fprintf(stderr, "blocksize: %d\n", blocksize);


  //Create the target file
  int target_fd = creat(target, 00777);  // ** I don't know how to cause an error if the file you are trying to create already exists

  if (target_fd == -1) {
    fprintf(stderr, "copyit: Couldn't create file %s: %s. \n", target, strerror(errno));
    exit(1);
  }


  int bytes_copied = 0;
  char buffer[blocksize];


  while (bytes_copied < filesize) {

    memset(buffer, 0, sizeof(buffer));
    //read from source file
    int read_return = read(source_fd, buffer, blocksize);
    while (errno == EINTR){
      read_return = read(source_fd, buffer, blocksize);
    }

    if (read_return == -1) {
      fprintf(stderr, "copyit: Couldn't read from file %s: %s.\n", source, strerror(errno));
      exit(1);
    }

    if (read_return == 0) {
      break;
    }

    //write to target file
    int write_return = write(target_fd, buffer, blocksize);
    while (errno == EINTR){
      write_return = write(target_fd, buffer, blocksize);
    }

    if (write_return == -1) {
      fprintf(stderr, "copyit: Couldn't write to file %s: %s. \n", target, strerror(errno));
    }

    if (read_return > write_return) {
      fprintf(stderr, "copyit: Read (%d) bytes from (%s), but only wrote (%d) bytes to (%s)\n", read_return, source, write_return, target);
      exit(1);
    }

    bytes_copied += read_return;

  }


  //Close files

  int close_return = close(source_fd);
  if (close_return == -1) {
    fprintf(stderr, "copyit: Couldn't close %s: %s\n" , source, strerror(errno));
    exit(1);
  }
  close_return = close(target_fd);
  if (close_return == -1) {
    fprintf(stderr, "copyit: Couldn't close %s: %s\n" , target, strerror(errno));
    exit(1);
  }

  fprintf(stderr, "copyit: Copied %d bytes from file %s to %s\n", bytes_copied, source, target);
  return 0;

}
