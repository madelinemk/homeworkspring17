// Madeline Kusters
// Operating System Project 2
// 2-10-17

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>


int start_cmd(char * args[]) {
  // start another program with command line arguments, print out the
  // process number of the running program

  //fork
  //  printf("Inside start... forking...\n");
  int pid = fork();

  if (pid < 0) {
    printf("myshell: Fork failed\n");
    exit(1);
  } else if (pid == 0) {
    // child
    // printf("Inside child process %d... execing...\n", pid);
    execvp(args[0], args);
  } else {
    // parent
    //return pid of the forked process
    printf("myshell: process %d started\n", pid);
    return pid;
  }

  // this should never be executed, but I want gcc to stop giving me a warning
  return -1;

}

void wait_cmd(int pid) {

  int exit_status;

  if (pid == 0) {
    pid = wait(&exit_status);
  } else {
    pid = waitpid(pid, &exit_status, 0);
  }

  if (pid == -1 && errno == ECHILD) {
    printf("myshell: no processes left\n");
    return;
  } else if (pid == -1) {
    printf("myshell: wait failed: %s\n", strerror(errno));
    return;
  }

  if (WIFEXITED(exit_status)) {
    printf("myshell: process %d exited normally with status %d\n", pid, WEXITSTATUS(exit_status));
    return;
  } else if (WIFSIGNALED(exit_status)) {
    printf("myshell: process %d exited abnormally with signal %d: %s\n", pid, WTERMSIG(exit_status), strsignal(WTERMSIG(exit_status)));
    return;
  } else {
    //this should not happen
    printf("myshell: process %d exited with status %d\n", pid, exit_status);
  }

}

void run_cmd(char * args[]) {
  int pid = start_cmd(args);

  wait_cmd(pid);

}

void kill_cmd(int pid, int signal) {

  int rc = kill(pid, signal);

  if (rc == -1) {
    printf("myshell: Error signaling process %d: %s\n", pid, strerror(errno));
  } else {
    switch (signal) {
    case SIGKILL:
      printf("myshell: process %d killed\n", pid);
      break;
    case SIGSTOP:
      printf("myshell: process %d stopped\n", pid);
      break;
    case SIGCONT:
      printf("myshell: process %d continued\n", pid);
      break;
    }
  }

}

void prompt() {
  printf("myshell> ");
  fflush(stdout);
}

int main(int argc, char * argv[]) {

  prompt();

  // accept input lines up to 4096 chars, 100 distinct words
  char input_buffer[4096];
  char *words[101];
  char *args[101];
  int i;

  // error checking for fgets?
  while( fgets(input_buffer, 4096, stdin) ) {
    // tokenize input string
    char * temp;
    // clear words from previous command
    temp = strtok(input_buffer, " \t\n");
    if (!temp) {
      prompt();
      continue;
    }

    words[0] = temp;
    int nwords = 1;
    while( (temp = strtok(0, " \t\n")) ) {
      words[nwords] = temp;
      nwords++;
    }
    words[nwords] = 0;

    // commands: start, wait, run, kill, stop, continue, quit, exit

    if (strcmp(words[0], "start") == 0) {

      // construct args
      for (i = 0; i < (nwords-1); i++) {
        args[i] = strdup(words[i+1]);
      }
      args[nwords-1] = 0;

      start_cmd(args);

    } else if (strcmp(words[0], "wait") == 0) {
      wait_cmd(0);

    } else if (strcmp(words[0], "run") == 0) {

      // construct args
      for (i = 0; i < (nwords-1); i++) {
        args[i] = strdup(words[i+1]);
      }
      args[nwords-1] = 0;

      run_cmd(args);

    } else if (strcmp(words[0], "kill") == 0) {

      if (nwords < 2) {
        printf("myshell: kill expects an integer argument\n \t usage: kill <pid>\n");
        prompt();
        continue;
      }

      kill_cmd(atoi(words[1]), SIGKILL);

    } else if (strcmp(words[0], "stop") == 0) {

      if (nwords < 2) {
        printf("myshell: stop expects an integer argument\n \t usage: stop <pid>\n");
        prompt();
        continue;
      }

      kill_cmd(atoi(words[1]), SIGSTOP);

    } else if (strcmp(words[0], "continue") == 0) {

      if (nwords < 2) {
        printf("myshell: continue expects an integer argument\n \t usage: continue <pid>\n");
        prompt();
        continue;
      }

      kill_cmd(atoi(words[1]), SIGCONT);


    } else if (strcmp(words[0], "quit") == 0) {
      return 0;

    } else if (strcmp(words[0], "exit") == 0) {
      return 0;
    } else {
      printf("myshell: unknown command: %s\n", words[0]);
    }

    prompt();

  }

  return 0;
}
