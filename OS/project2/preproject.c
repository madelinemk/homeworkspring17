#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char * argv[]) {

  if (argc != 2) {
    fprintf(stderr, "preproject: Wrong number of arguments!\nusage: preproject <source>\n");
    exit(1);
  }

  // Fork process to run cp
  int pid = fork();

  if (pid < 0) {
    fprintf(stderr, "preproject: Fork Failed\n");
    exit(1);
  } else if (pid == 0) {
    // Child process
    fprintf(stderr, "In child process, pid: %d\n", (int) getpid());
    // Exec and run cp
    char * cp_args[4];
    cp_args[0] = strdup("cp");
    cp_args[1] = strdup(argv[1]);
    cp_args[2] = strdup("CloneFile");
    cp_args[3] = NULL;
    execvp(cp_args[0], cp_args);
  } else {
    // Parent process
    fprintf(stderr, "In parent process, parent pid: %d, child pid: %d\n", (int) getpid(), pid);
    // wait
    fprintf(stderr, "Waiting for child process...\n");
    int child_status = wait(NULL);
    fprintf(stderr, "Child process complete.\n");
    // exec to run md5sum
    fprintf(stderr, "Running md5sum to verify correctness of CloneFile:\n\n");
    char * md5sum_args[4];
    md5sum_args[0] = strdup("md5sum");
    md5sum_args[1] = strdup(argv[1]);
    md5sum_args[2] = strdup("CloneFile");
    md5sum_args[3] = NULL;
    execvp(md5sum_args[0], md5sum_args);
  }

}
